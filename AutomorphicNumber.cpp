#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<math.h>
using namespace std;

//自守数是指一个数的平方的尾数等于该数自身的自然数。
//例如：25 ^ 2 = 625，76 ^ 2 = 5776，9376 ^ 2 = 87909376。
//如果一个数是自守数，那么它的平方减去它，对pow(10, 这个数的位数)求余一定为0

bool Judge(int x)      //判断一个数是否为自守数
{
    int square = pow(x, 2);
    string s = to_string(x);
    int divisor = pow(10, s.size());
    if ((square - x) % divisor == 0)
        return true;
    return false;
}

int NumOfVal(int n)     //遍历一遍从0到n的每个数
{
    int count = 0;
    for (int i = 0; i <= n; ++i)
    {
        if (Judge(i))
            ++count;
    }
    return count;
}

int main()
{
    int n = 0;
    while (cin >> n)
    {
        cout << NumOfVal(n) << endl;
    }
    return 0;
}