#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

bool CheckPermutation(string s1, string s2)
{
	if (s1.size() != s2.size())
		return false;
	int arr[128] = { 0 };
	for (int i = 0; i < s1.size(); ++i)
	{
		arr[s1[i]]++;
		arr[s2[i]]--;
	}
	for (int i = 0; i < s1.size(); ++i)
	{
		if (arr[s1[i]] != 0)
			return false;
	}
	return true;
}

int main(void)
{
	string s1, s2;
	cin >> s1;
	cin >> s2;
	cout << CheckPermutation(s1, s2) << endl;
	return 0;
}