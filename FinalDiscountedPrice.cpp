#define _CRT_SECURE_NO_WARNINGS 1
#include<stack>
#include<vector>
using namespace std;

class Solution {
public:
    
    vector<int> FinalDiscountedPrice(vector<int>& prices) {
        stack<int> st;
        st.push(0);
        for (int i = 1; i < prices.size(); ++i)
        {
            if (prices[i] <= prices[st.top()])
            {
                while (!st.empty() && prices[i] <= prices[st.top()])
                {
                    prices[st.top()] -= prices[i];
                    st.pop();
                }
            }
            st.push(i);
        }
        return prices;
    }
};