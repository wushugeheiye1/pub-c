#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
using namespace std;

int FirstNotRepeatingChar(string str)
{
	int arr[128] = { 0 };
	for (auto e : str)
	{
		++arr[e];
	}
	for (int i = 0; i < str.size(); ++i)
	{
		if (arr[str[i]] == 1)
			return i;
	}
	return -1;
}

int main(void)
{
	string str;
	cin >> str;
	cout << FirstNotRepeatingChar(str) << endl;
	return 0;
}
