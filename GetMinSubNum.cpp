#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

int GetMinSubNum(const vector<int>& v)
{
    int count = 0;
    int i = 0;
    while (i < v.size())
    {
        if (v[i] < v[i + 1])
        {
            while (i < v.size() && v[i] <= v[i + 1])
            {
                ++i;
            }
            ++count;
            ++i;
        }
        else if (v[i] == v[i + 1])
        {
            ++i;
        }
        else
        {
            while (i < v.size() && v[i] >= v[i + 1])
            {
                ++i;
            }
            ++count;
            ++i;
        }
    }
    return count;
}

int main(void)
{
    int n = 0;
    cin >> n;
    vector<int> v;
    v.resize(n);
    for (int i = 0; i < n; ++i)
    {
        cin >> v[i];
    }
    cout << GetMinSubNum(v) << endl;
    return 0;
}