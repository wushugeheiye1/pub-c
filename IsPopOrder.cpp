#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<stack>
using namespace std;

bool IsPopOrder(vector<int> pushV, vector<int> popV)
{
	stack<int> _st;
	size_t pushi = 0, popi = 0;
	while(pushi < pushV.size())
	{
		_st.push(pushV[pushi++]);
		while (!_st.empty() && _st.top() == popV[popi])
		{
			++popi;
			_st.pop();
		}
	}
	return _st.empty();
}

int main(void)
{
	vector<int> pushV = { 1,2,3,4,5 };
	vector<int> popV = { 4,3,5,1,2 };
	cout << IsPopOrder(pushV, popV) << endl;
	return 0;
}
