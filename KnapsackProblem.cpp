#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

int MaxValue(const vector<int>& weight, const vector<int>& value, int n, int m)
{
	int** dp = new int* [n + 1];
	for (int i = 0; i < (n + 1); ++i)
	{
		dp[i] = new int[m + 1]();
	}
	for (int i = 1; i <= n; ++i)
	{
		for (int j = 1; j <= m; ++j)
		{
			if (weight[i - 1] > j)
				dp[i][j] = dp[i - 1][j];
			else
			{
				dp[i][j] = max(dp[i - 1][j], dp[i - 1][j - weight[i - 1]] + value[i - 1]);
			}
		}
	}
	return dp[n][m];
}

int main(void)
{
	int n = 0, m = 0;
	cout << "请输入物品的个数：" << endl;
	cin >> n;
	cout << "请输入背包的容量：" << endl;
	cin >> m;
	vector<int> weight, value;
	weight.resize(n);
	value.resize(n);
	cout << "请输入每个物品的大小：" << endl;
	for (int i = 0; i < n; ++i)
		cin >> weight[i];
	cout << "请输入每个物品的价值：" << endl;
	for (int i = 0; i < n; ++i)
		cin >> value[i];
	int ret = MaxValue(weight, value, n, m);
	cout << "背包所能容纳最大价值为：" << ret <<  endl;
	return 0;
}