#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
#define MAX 999

int deltaX[] = { -2, -1, 1, 2 };
int deltaY[] = { -1, -2, -2, -1 };

int MinPath(const vector<vector<int>>& checkerd)
{
	if (checkerd.size() == 0)
		return -1;
	int row = checkerd.size();
	int col = checkerd[0].size();
	//动态开辟二维数组
	int** dp = new int* [row];
	for (int i = 0; i < row; ++i)
	{
		dp[i] = new int[3]();
	}
	//第一列置为MAX
	for (int i = 0; i < row; ++i)
	{
		dp[i][0] = MAX;
	}
	dp[0][0] = 0;	//第一个元素初始化
	for (int j = 1; j < col; ++j)
	{
		for (int i = 0; i < row; ++i)
		{
			//重复利用空间，重新初始化为MAX
			dp[i][j % 3] = MAX;
			//判断可以到达这个点的四个方向
			for (int dir = 0; dir < 4; ++dir)
			{
				int x = i + deltaX[dir];
				int y = j + deltaY[dir];
				if (x < 0 || x >= row || y < 0 || y >= col)
					continue;
				if (dp[x][y % 3] == MAX)
					continue;
				dp[i][j % 3] = min(dp[i][j % 3], dp[x][y % 3] + 1);
			}
		}
	}
	if (dp[row - 1][(col - 1) % 3] == MAX)
		return -1;
	return dp[row - 1][(col - 1) % 3];
}

int main(void)
{
	vector<vector<int>> checkerd;
	int row = 0, col = 0;
	cout << "请输入棋盘的行数和列数：" << endl;
	cin >> row >> col;
	checkerd.resize(row);
	for (int i = 0; i < checkerd.size(); ++i)
	{
		checkerd[i].resize(col);
	}
	int ret = MinPath(checkerd);
	cout << "最短路径为：" << ret << endl;
	return 0;
}