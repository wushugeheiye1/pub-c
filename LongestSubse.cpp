#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<algorithm>
#include<string>
using namespace std;

int LongestSubseq(const vector<int>& v)
{
	if (v.size() == 0)
		return 0;
	int* dp = new int[v.size()]();
	for (int i = 0; i < v.size(); ++i)
		dp[i] = 1;
	for (int i = 1; i < v.size(); ++i)
	{
		for (int j = 0; j < i; ++j)
		{
			if (v[j] < v[i])
			{
				dp[i] = max(dp[j] + 1, dp[i]);
			}
		}
	}
	int maxi = dp[0];
	for (int i = 1; i < v.size(); ++i)
	{
		if (dp[i] > maxi)
			maxi = dp[i];
	}
	return maxi;
}

int main(void)
{
	vector<int> v;
	cout << "请输入数据：" << endl;
	int n = 0;
	while (cin >> n)
	{
		v.push_back(n);
		if (cin.get() == '\n')
			break;
	}
	int ret = LongestSubseq(v);
	cout << "最长上升子序列为：" << ret << endl;
	return 0;
}