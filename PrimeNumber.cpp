#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

bool JudgePriNum(int x)     //判断一个数是否为质数
{
    for (int i = 2; i < x; ++i)
    {
        if (x % i == 0)
            return false;
    }
    return true;
}

int NumOfPriNum(int n)      //依次遍历(除0和1以外)每个数
{
    int count = 0;
    if (n < 2)
        return count;
    for (int i = 2; i < n; ++i)
    {
        if (JudgePriNum(i))
            ++count;
    }
    return count;
}

int main(void)
{
    int n = 0;
    cin >> n;
    cout << NumOfPriNum(n) << endl;
    return 0;
}