#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<algorithm>
#include<functional>
using namespace std;
#define ROWC 4

int MinPathSum(const vector<vector<int>>& vv)
{
	if (vv.size() == 0)
		return -1;
	int dp[2][ROWC] = { 0 };
	dp[0][0] = vv[0][0];
	for (int i = 1; i < ROWC; ++i)
	{
		dp[i % 2][0] = dp[(i - 1) % 2][0] + vv[i][0];
		dp[i % 2][i] = dp[(i - 1) % 2][i - 1] + vv[i][i];
		for (int j = 1; j < i; ++j)
		{
			dp[i % 2][j] = min(dp[(i - 1) % 2][j - 1], dp[(i - 1) % 2][j]) + vv[i][j];
		}
	}
	int mini = dp[(ROWC - 1) % 2][0];
	for (int i = 1; i < ROWC; ++i)
	{
		if (dp[(ROWC - 1) % 2][i] < mini)
		{
			mini = dp[(ROWC - 1) % 2][i];
		}
	}
	return mini;
}

int main(void)
{
	vector<vector<int>> vv;
	cout << "请输入三角形：" << endl;
	vv.resize(ROWC);
	for (int i = 0; i < vv.size(); ++i)
	{
		vv[i].resize(ROWC);
	}
	for (int i = 0; i < ROWC; ++i)
	{
		for (int j = 0; j <= i; ++j)
		{
			cin >> vv[i][j];
		}
	}
	int ret = MinPathSum(vv);
	cout << "最小路径和为：" << ret << endl;
	return 0;
}