#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
using namespace std;

string WordAbbr(string s)
{
    if (s.size() < 10)
        return s;
    string ret = "";
    int sum = s.size() - 2;
    string str = to_string(sum);
    ret += s[0] + str + s[s.size() - 1];
    return ret;
}

int main(void)
{
    int n = 0;
    cin >> n;
    string s;
    while (cin >> s)
    {
        cout << WordAbbr(s) << endl;
    }
    return 0;
}