#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

int bitSwapRequired(int a, int b)
{
	int c = a ^ b;
	int count = 0;
	for (int i = 0; i < 32; ++i)
	{
		if (((c >> i) & 1) == 1)
			++count;
	}
	return count;
}

int main(void)
{
	int a = 0, b = 0;
	cin >> a >> b;
	cout << bitSwapRequired(a, b) << endl;
	return 0;
}