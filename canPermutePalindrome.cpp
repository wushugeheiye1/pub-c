#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
using namespace std;

bool canPermutePalindrome(string s)
{
	if (s.size() < 2)
		return 0;
	int count = 0;
	int arr[128] = { 0 };
	for (int i = 0; i < s.size(); ++i)
	{
		if (arr[s[i]] == 0)
			arr[s[i]]++;
		else
			arr[s[i]]--;
	}
	for (int i = 0; i < 128; ++i)
	{
		if (arr[i] == 1)
			++count;
	}
	if (count == 0 || count == 1)
		return true;
	return false;
}

int main(void)
{
	string s;
	cin >> s;
	cout << canPermutePalindrome(s) << endl;
	return 0;
}