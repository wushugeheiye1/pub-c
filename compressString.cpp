#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
using namespace std;

string compressString(string S) {
    string str = "";
    int count = 1;
    for (int i = 1; i <= S.size(); ++i)
    {
        if (S[i] != S[i - 1])
        {
            str += (S[i - 1] + to_string(count));
            count = 1;
        }
        else
        {
            ++count;
        }
    }
    return str.size() < S.size() ? str : S;
}

int main(void)
{
    string s;
    cin >> s;
    cout << compressString(s) << endl;
	return 0;
}