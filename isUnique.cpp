#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//前提条件字符串只有26个小写字母
// mark = 0   00000000 00000000 00000000 00000000
// 1          00000000 00000000 00000000 00000001
// 依次判断字符串中的每一个字符
// 例如astr第一个字符为'c'
// 'c' - 'a' 为2
// 1左移两位   00000000 00000000 00000000 00000100
// mark       00000000 00000000 00000000 00000000
// 用这个结果去和mark进行与运算，因为1的32位只有一个1，
// 所以用(1 << ('c' - 'a'))的方式就可以唯一标识字母c
// 如果与的结果为0，说明这bit位还是0，这个字母是第一次出现
// 那么用或运算，mark = mark | （1 << ('c' - 'a'))，或运算有一个1就是1
// 用或运算将这一位变成1，表示这个字母出现一次了
// mark 00000000 00000000 00000000 00000100
// 如果下次字母c再出现
// (1 << ('c' - 'a')) 00000000 00000000 00000000 00000100
// mark				  00000000 00000000 00000000 00000100
// 二者相与的结果就为1，说明此时已经是第2个字母c了，重复了，return false即可。

bool isUnique(string astr)
{
	int mark = 0;
	for (int i = 0; i < astr.size(); ++i)
	{
		if (mark & (1 << (astr[i] - 'a')))
			return false;
		mark |= (1 << (astr[i] - 'a'));
	}
	return true;
}

int main(void)
{
	string str;
	cin >> str;
	cout << isUnique(str) << endl;
	return 0;
}