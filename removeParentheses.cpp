#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<stack>
#include<string>
using namespace std;

class Solution {
public:
    
    string removeParentheses(string& s) {
        stack<int> st;
        int n = s.size();
        vector<int> flag(n);
        for (int i = 0; i < n; ++i)
        {
            if (s[i] == '(')
            {
                st.push(i);
            }
            else if (s[i] == ')')
            {
                if (st.size() == 0)
                {
                    flag[i] = 1;
                }
                else
                {
                    st.pop();
                }
            }
        }
        while (!st.empty())
        {
            flag[st.top()] = 1;
            st.pop();
        }
        string ret = "";
        for (int i = 0; i < n; ++i)
        {
            if (flag[i] == 0)
                ret += s[i];
        }
        return ret;
    }
};