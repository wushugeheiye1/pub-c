#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
using namespace std;

string replaceSpaces(string s, int length)
{
	int count = 0;
	for (int i = 0; i < length; ++i)
	{
		if (s[i] == ' ')
			++count;
	}
	int new_size = 2 * count + length;
	int pos = new_size - 1;
	for (int i = length - 1; i >= 0; --i)
	{
		if (s[i] == ' ')
		{
			s[pos--] = '0';
			s[pos--] = '2';
			s[pos--] = '%';
		}
		else
		{
			s[pos--] = s[i];
		}
	}
	s[new_size] = '\0';
	return s;
}

int main(void)
{
	string s;
	int length = 0;
	getline(cin, s);
	cin >> length;
	cout << replaceSpaces(s, length) << endl;
}
