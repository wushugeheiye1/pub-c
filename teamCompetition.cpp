#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

int main(void)
{
	int n;
	while (cin >> n)
	{
		long long sum = 0;
		vector<int> v;
		v.resize(3 * n);
		for (int i = 0; i < (3 * n); ++i)
		{
			cin >> v[i];
		}
		sort(v.begin(), v.end());
		for (int i = 0; i < n; ++i)
		{
			sum += v[v.size() - 2 * (i + 1)];
		}
		cout << sum << endl;
	}
	return 0;
}