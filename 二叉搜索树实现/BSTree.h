#pragma once
#include<iostream>
using namespace std;

namespace KEY
{
	template<class K>
	struct BSTreeNode
	{
		BSTreeNode<K>* _left;
		BSTreeNode<K>* _right;

		K _key;
		BSTreeNode(const K& key)
			:_left(nullptr)
			, _right(nullptr)
			, _key(key)
		{}
	};

	template<class K>
	class BSTree
	{
		typedef BSTreeNode<K> Node;
	public:
		BSTree()
			:_root(nullptr)
		{}

		BSTree(const BSTree<K>& t)
		{
			_root = _Copy(t._root);
		}

		BSTree<K>& operator=(BSTree<K> t)
		{
			std::swap(_root, t._root);
			return *this;
		}

		~BSTree()
		{
			_Destroy(_root);
		}

		bool Insert(const K& key)
		{
			// 根为空
			if (_root == nullptr)
			{
				_root = new Node(key);
				return true;
			}
			// 根不为空
			// 用parent节点记录要插入节点的父节点
			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				// 根节点的值小于要插入位置的值，去右边找
				// 同时记录父节点方便找到插入位置后链接新节点
				if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}	// 根节点的值大于要插入位置的值，去左边找
				else if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					// 要插入的值已经存在
					return false;
				}
			}
			cur = new Node(key);
			// 将新节点链接
			// 如果新节点的值比父节点的值大，根据二叉搜索树性质，它应该在父亲的右边
			// 反之在父亲的左边
			if (parent->_key < cur->_key)
				parent->_right = cur;
			else
				parent->_left = cur;
		}

		Node* Find(const K& key)
		{
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key < key)
					cur = cur->_right;
				else if (cur->_key > key)
					cur = cur->_left;
				else    // 找到了返回该节点
					return cur;
			}
			// 没找到返回空
			return nullptr;
		}

		bool Erase(const K& key)
		{
			// 记录下要删除节点的父节点
			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				// 找到要删除的节点
				if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					// 要删除的节点只有右孩子或者叶子节点，左边为空
					if (cur->_left == nullptr)
					{
						if (cur == _root)
						{
							_root = cur->_right;
						}
						else
						{
							// 如果要删除的节点在父亲的左边，让父亲的左边指向它的右
							// 如果要删除的节点在父亲的右边，让父亲的右边指向它的右
							if (cur == parent->_left)
								parent->_left = cur->_right;
							else
								parent->_right = cur->_right;
						}
						delete cur;
					}
					else if (cur->_right == nullptr)	// 只有左孩子，右边为空
					{
						if (cur == _root)
						{
							_root = cur->_left;
						}
						else
						{
							if (cur == parent->_left)
								parent->_left = cur->_left;
							else
								parent->_right = cur->_left;
						}
						delete cur;
					}
					else    // 左右孩子都有
					{
						Node* minRightParent = cur;		//记录右子树最小节点的父节点
						Node* minRight = cur->_right;	//记录右子树最小节点
						while (minRight->_left)		// 找到右子树最小节点
						{
							minRightParent = minRight;
							minRight = minRight->_left;
						}
						// 将右子树最小节点的值赋值给要删除节点的值
						cur->_key = minRight->_key;
						// 改变最小节点的父节点指针指向
						if (minRight == minRightParent->_left)
							minRightParent->_left = minRight->_right;
						else
							minRightParent->_right = minRight->_right;
						// 删除右边最小节点
						delete minRight;
					}
					return true;
				}
			}
			// 没有要删除的节点
			return false;
		}

		void Inorder()
		{
			_Inorder(_root);
			cout << endl;
		}

		bool InsertR(const K& key)
		{
			return _InsertR(_root, key);
		}

		Node* FindR(const K& key)
		{
			return _FindR(_root, key);
		}

		bool EraseR(const K& key)
		{
			return _EraseR(_root, key);
		}

	private:
		Node* _Copy(Node* root)
		{
			if (root == nullptr)
				return nullptr;
			Node* newRoot = new Node(root->_key);
			newRoot->_left = _Copy(root->_left);
			newRoot->_right = _Copy(root->_right);
			return newRoot;
		}

		void _Destroy(Node* root)
		{
			if (root == nullptr)
				return;
			_Destroy(root->_left);
			_Destroy(root->_right);
			delete root;
		}

		bool _EraseR(Node*& root, const K& key)
		{
			if (root == nullptr)
				return false;
			if (root->_key < key)
			{
				return _EraseR(root->_right, key);
			}
			else if (root->_key > key)
			{
				return _EraseR(root->_left, key);
			}
			else
			{
				Node* del = root;
				if (root->_left == nullptr)
					root = root->_right;
				else if (root->_right == nullptr)
					root = root->_left;
				else
				{
					Node* minRight = root->_right;
					while (minRight->_left)
						minRight = minRight->_left;
					root->_key = minRight->_key;
					return _EraseR(root->_right, minRight->_key);
				}
				delete del;
				return true;
			}
		}

		Node* _FindR(Node* root, const K& key)
		{
			if (root == nullptr)
				return nullptr;
			if (root->_key < key)
				return _FindR(root->_right, key);
			else if (root->_key > key)
				return _FindR(root->_left, key);
			else
				return false;
		}

		bool _InsertR(Node*& root, const K& key)
		{
			if (root == nullptr)
			{
				root = new Node(key);
				return true;
			}
			if (root->_key < key)
			{
				return _InsertR(root->_right, key);
			}
			else if (root->_key > key)
			{
				return _InsertR(root->_left, key);
			}
			else
			{
				return false;
			}
		}

		void _Inorder(Node* root)
		{
			if (root == nullptr)
				return;
			_Inorder(root->_left);
			cout << root->_key << " ";
			_Inorder(root->_right);
		}

	private:
		Node* _root = nullptr;
	};
}

namespace KEY_VALUE
{
	template<class K, class V>
	struct BSTreeNode
	{
		BSTreeNode<K, V>* _left;
		BSTreeNode<K, V>* _right;
		K _key;
		V _value;

		BSTreeNode(const K& key, const V& value)
			:_left(nullptr)
			, _right(nullptr)
			, _key(key)
			, _value(value)
		{}
	};

	template<class K, class V>
	class BSTree
	{
		typedef BSTreeNode<K, V> Node;
	public:
		V& operator[](const K& key)
		{
			pair<Node*, bool> ret = Insert(key, V());
			return ret.first->_value;
		}

		pair<Node*, bool> Insert(const K& key, const V& value)
		{
			if (_root == nullptr)
			{
				_root = new Node(key, value);
				return make_pair(_root, true);
			}
			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					return make_pair(cur, false);
				}
			}
			cur = new Node(key, value);
			if (parent->_key < cur->_key)
				parent->_right = cur;
			else
				parent->_left = cur;
			return make_pair(cur, true);
		}

		Node* Find(const K& key)
		{
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key < key)
					cur = cur->_right;
				else if (cur->_key > key)
					cur = cur->_left;
				else
					return cur;
			}
			return nullptr;
		}

		bool Erase(const K& key)
		{
			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					if (cur->_left == nullptr)
					{
						if (cur == _root)
						{
							_root = cur->_right;
						}
						else
						{
							if (cur == parent->_left)
								parent->_left = cur->_right;
							else
								parent->_right = cur->_right;
						}
						delete cur;
					}
					else if (cur->_right == nullptr)
					{
						if (cur == _root)
						{
							_root = cur->_left;
						}
						else
						{
							if (cur == parent->_left)
								parent->_left = cur->_left;
							else
								parent->_right = cur->_left;
						}
						delete cur;
					}
					else
					{
						Node* minRightParent = cur;
						Node* minRight = cur->_right;
						while (minRight->_left)
						{
							minRightParent = minRight;
							minRight = minRight->_left;
						}
						cur->_key = minRight->_key;
						if (minRight == minRightParent->_left)
							minRightParent->_left = minRight->_right;
						else
							minRightParent->_right = minRight->_right;
						delete minRight;
					}
					return true;
				}
			}
			return false;
		}

		void Inorder()
		{
			_Inorder(_root);
			cout << endl;
		}

	private:
		void _Inorder(Node* root)
		{
			if (root == nullptr)
				return;
			_Inorder(root->_left);
			cout << root->_key << " : " << root->_value << endl;;
			_Inorder(root->_right);
		}

	private:
		Node* _root = nullptr;
	};
}