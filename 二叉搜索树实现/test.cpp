#define _CRT_SECURE_NO_WARNINGS 1
#include <string>
#include "BSTree.h"

void TestBSTreeKEY()
{
	int a[] = { 5,3,4,1,7,8,2,6,0,9 };
	KEY::BSTree<int> bst;
	for (auto& e : a)
	{
		bst.InsertR(e);
	}
	bst.Inorder();
	KEY::BSTree<int> copy = bst;
	copy.Inorder();
}

void TestBSTreeKEY_VALUE()
{
	KEY_VALUE::BSTree<string, string> dict;
	dict.Insert("sort", "����");
	dict.Insert("insert", "����");
	dict.Insert("right", "�ұ�");
	dict.Insert("tree", "��");
	dict.Inorder();
	dict.Erase("insert");
	dict.Inorder();
}

int main(void)
{
	//TestBSTreeKEY();
	TestBSTreeKEY_VALUE();
	return 0;
}